const PORT = process.env.PORT || 3000;
const http = require('http');
const WebSocket = require('ws');

const secret = 'mysecret';

const httpServer = http.createServer((request, response) => {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Welcome to wsevents app')
});

const wsServer = new WebSocket.Server({ server: httpServer });

wsServer.on('connection', ws => {
    ws.on('message', data => {
        let msg = JSON.parse(data);

        console.log('Message: ' + data);

        // broadcast message
        if (msg.key && msg.key == secret) {
            delete msg.key;
            wsServer.clients.forEach(client => {
                if (client !== ws && client.readyState === WebSocket.OPEN)
                    client.send(JSON.stringify(msg));
            });     
        }
    });
});

wsServer.on('close', _ => {
    console.log('Connection closed')
});

console.log('Server listening on port ' + PORT);

httpServer.listen(PORT)
