# sync-web-presentations

Simple web pages to presenter and attendees for synchronized presentations.

It use websockets to send messages from presenter to attendees.
You can serve presentation content (for now, svg files) from any static web
server ([gitlab], [github], Google drive/site, ...).

The `server.js` file implements a simple broadcasting service from presenter to
attendees based on [node.js] using 
[websockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API).

## How it works

- Presenter can load the `presenter.html` web page from its own webserver 
  (even at your local computer).
- Presenter have to edit the file `content.json` to set presentation title, 
  author, and the list of slides and slide notes (see example).
- The websocket application (`server.js`) can run on some application server
  like [heroku](https://www.heroku.com/) or any other platform.
- You have to _publish_ the files `presentation.html`, `content.json` and svg
  files (slides) to attendees in any web service
- Distribute the URL for attendees access (example:
  https://marceloarroyo.gitlab.io/mytalk/presentation.html)
- Enjoy! Presenter can advance slides and attendees view will update with
  current slide.
